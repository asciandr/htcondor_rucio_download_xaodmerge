#!/bin/bash
PWD=$(pwd)

listtxt=$*
COUNTER=0
while read r_command
do
    echo "RUCIO COMMAND:"
    echo ${r_command}
    cat BATCH_job.sh | sed "s|XRUCIO_COMMAND|${r_command}|g" | sed "s|XCOUNTER|${COUNTER}|g" | sed "s|XFOLDER|${PWD}|g" > BATCH_job_${COUNTER}.sh
    cat hello.sub | sed "s|XCOUNTER|${COUNTER}|g" > hello_${COUNTER}.sub
    chmod 755 BATCH_job_${COUNTER}.sh
    chmod 755 hello_${COUNTER}.sub
    condor_submit hello_${COUNTER}.sub
    COUNTER=$[${COUNTER}+1]
done < $*


