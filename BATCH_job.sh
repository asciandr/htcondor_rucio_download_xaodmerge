#!/bin/bash

echo "Define working dirs and cp input files (if any)."
export DIR=/afs/cern.ch/user/a/asciandr/HADD/hadded_data/
export WORK_DIR=/afs/cern.ch/user/a/asciandr/work/
#export PWD=XFOLDER
#cp ${PWD}/xAODPOOLMerge.py .
echo "ATLAS setup."
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
#setupATLAS -c centos7
echo "voms setup."
### IF ON THE GRID 
ls -haltr $WORK_DIR/grid_proxy
cp $WORK_DIR/grid_proxy /tmp/x509up_u76083
ls -haltr /tmp/x*
chmod 600 /tmp/x509up_u76083
voms-proxy-init -voms atlas -n
#export RUCIO_ACCOUNT=asciandr
echo "rucio setup and download."
lsetup "rucio -w"
#ls -haltr $DIR
#echo "RUCIO_ACCOUNT="
#echo $RUCIO_ACCOUNT
XRUCIO_COMMAND
ls -haltr
echo "Download done."
### already deleted in the txt!
#find . -name "*.root" -type 'f' -size -200k -delete
#echo "Removed small expty files. Set athena up and merge files."
rm *.root.part
echo "Removed unsuccessful downloads"
ls -haltr
asetup Athena,22.0.41
xAODMerge group.perf-idtracking.27309811.EXT0._XCOUNTER.DAOD_IDTIDE.pool.root group.perf-idtracking.27309811.EXT0.*
echo "Merge within athena done."
# N.B. hadd DOES NOT WORK WITH POOL FILES (such as xAODs and DxAODs)
#hadd -f2 group.perf-idtracking.27309811.EXT0._XCOUNTER.DAOD_IDTIDE.pool.root *.root
#echo "hadd done."
cp group.perf-idtracking.27309811.EXT0._XCOUNTER.DAOD_IDTIDE.pool.root $DIR/
echo "Hadded file copied."
